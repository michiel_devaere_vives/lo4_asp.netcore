﻿using System.Collections.Generic;
using System.Linq;
using LO4.Models;
using Microsoft.Extensions.Configuration;
using MongoDB.Bson;
using MongoDB.Driver;

namespace LO4.Services
{
    public class UserService
    {
        private readonly IMongoCollection<User> _users;

        public UserService(IConfiguration config)
        {
            var client = new MongoClient(config.GetConnectionString("myDB"));
            var database = client.GetDatabase("myDB");
            _users = database.GetCollection<User>("user");
        }

        public List<User> Get()
        {
            return _users.Find(user => true).ToList();
        }

        public User Get(string id)
        {
            var docId = new ObjectId(id);

            return _users.Find<User>(book => book.Id == docId).FirstOrDefault();
        }

        public User Create(User pUser)
        {
            _users.InsertOne(pUser);
            return pUser;
        }

        public void Update(string id, User UserIn)
        {
            var docId = new ObjectId(id);

            _users.ReplaceOne(book => book.Id == docId, UserIn);
        }

        public void Remove(User userIn)
        {
            _users.DeleteOne(user => user.Id == userIn.Id);
        }

        public void Remove(ObjectId id)
        {
            _users.DeleteOne(user => user.Id == id);
        }
    }
}
