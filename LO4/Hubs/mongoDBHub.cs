﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;

namespace LO4.Hubs
{
    public class MongoDBHub: Hub
    {
        public async Task SendMessage(string pName, string pPassword)
        {
            await Clients.All.SendAsync("ReceiveMessage", pName, pPassword);
        }
    }
}
